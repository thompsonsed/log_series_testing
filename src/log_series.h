//
// Created by Sam Thompson on 01/11/2018.
//

#ifndef EFFICIENT_COALESCENCE_LOG_SERIES_H
#define EFFICIENT_COALESCENCE_LOG_SERIES_H

#include <random>
#include "Rcpp.h"

// [[Rcpp::export]]
Rcpp::NumericVector genRandomLogarithmicLK(const unsigned long &number, const double &alpha);

double genRandomLogarithmicLK(const double &alpha, std::uniform_real_distribution<double> &real_distribution,
							  std::default_random_engine &generator);

// [[Rcpp::export]]
Rcpp::NumericVector genRandomLogarithmicLK2(const unsigned long &number, const double &alpha);

double genRandomLogarithmicLK2(const double &alpha, std::uniform_real_distribution<double> &real_distribution,
							  std::default_random_engine &generator);
#endif //EFFICIENT_COALESCENCE_LOG_SERIES_H
