//
// Created by Sam Thompson on 01/11/2018.
//

#include "log_series.h"
// [[Rcpp::plugins(cpp14)]]
//' @useDynLib logseriestesting, .registration=TRUE


Rcpp::NumericVector genRandomLogarithmicLK(const unsigned long &number, const double &alpha)
{
	Rcpp::NumericVector out(number);
	std::default_random_engine generator;
	std::uniform_real_distribution<double> real_distribution(0.0, 1.0);
	for(unsigned long i = 0; i < number; i ++)
	{
		out[i] = (genRandomLogarithmicLK(alpha, real_distribution, generator));
	}
	return out;
}

double genRandomLogarithmicLK(const double &alpha, std::uniform_real_distribution<double> &real_distribution,
							  std::default_random_engine &generator)
{
	double u_2 = real_distribution(generator);
	if(u_2 > alpha)
	{
		return 1;
	}
	long double h = log(1 - alpha);
	long double q = 1 - exp(real_distribution(generator) * h);
	if(u_2 < (q * q))
	{
		return static_cast<unsigned long>(floor(1 + log(u_2) / log(q)));
	}
	else if(u_2 > q)
	{
		return 1;
	}
	else
	{
		return 2;
	}
}

Rcpp::NumericVector genRandomLogarithmicLK2(const unsigned long &number, const double &alpha)
{
	Rcpp::NumericVector out(number);
	std::default_random_engine generator;
	std::uniform_real_distribution<double> real_distribution(0.0, 1.0);
	for(unsigned long i = 0; i < number; i ++)
	{
		out[i] = (genRandomLogarithmicLK2(alpha, real_distribution, generator));
	}
	return out;
}

// This is the error version
double genRandomLogarithmicLK2(const double &alpha, std::uniform_real_distribution<double> &real_distribution,
							  std::default_random_engine &generator)
{
	double u_2 = real_distribution(generator);
	if(u_2 > alpha)
	{
		return 2;
	}
	long double h = log(1 - alpha);
	long double q = 1 - exp(real_distribution(generator) * h);
	if(u_2 < (q * q))
	{
		return static_cast<unsigned long>(round(1 + log(u_2) / log(q))); // this should be floor(not round)
	}
	else if(u_2 > q)
	{
		return 2; // This should be 1
	}
	else
	{
		return 1; // this should be 2
	}

}
