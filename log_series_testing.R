# Logarithmic function testing
library(logseriestesting)
library(tidyverse)
library(actuar)
library(viridis)
# Randomly generate numbers from a log series using different methods
random_data <- data.frame(table(rlogarithmic(n = 1000000, 1-0.00001))) %>% 
  rename(abundance=Var1, random_richness1=Freq) %>% 
  mutate(random_richness1 = random_richness1/sum(random_richness1),
         abundance=as.numeric(abundance))
random_data2 <- data.frame(table(genRandomLogarithmicLK(1000000, 1-0.00001))) %>% 
  rename(abundance=Var1, random_richness2=Freq) %>% 
  mutate(random_richness2 = random_richness2/sum(random_richness2),
         abundance=as.numeric(abundance))
random_data3 <- data.frame(table(genRandomLogarithmicLK2(1000000, 1-0.00001))) %>% 
  rename(abundance=Var1, random_richness3=Freq) %>% 
  mutate(random_richness3 = random_richness3/sum(random_richness3),
         abundance=as.numeric(abundance))
# Provide the analytical expectation of the distribution of random numbers
analytical_data <- expand.grid(abundance=seq(1, 100000, 1)) %>%
  mutate(analytical_richness =dlogarithmic(abundance, 1-0.00001))

# Combine the different methods into one object
df_combined <- random_data %>% full_join(analytical_data) %>% full_join(random_data2) %>%
  full_join( random_data3) %>%
  rename(random_actuar = random_richness1, random_thompson = random_richness2,
         random_thompson2=random_richness3) %>%
  gather(key="source", value = "density",  analytical_richness, random_actuar, random_thompson,
         random_thompson2)

# Generate the plots of the different methods
p <- ggplot(df_combined )+
  geom_line(aes(x=abundance, y=density, colour=source, linetype=source)) +
  scale_colour_viridis("", discrete=TRUE, labels=c("analytical", "actuar random", "LK random",
                                                   "LK random (modified)")) +
  scale_linetype_discrete("", labels=c("analytical", "actuar random", "LK random",
                                       "LK random (modified)")) +
  theme_classic() + scale_x_log10(limits=c(1,100))+ ylab("Density") + 
  xlab("LK-generated random number")
if(!dir.exists("figures"))
{
  dir.create("figures")
}
pdf(file.path("figures", "log_series_gen_comparison.pdf"), 6, 4)
print(p)
dev.off()



